<?php
include "header.php";
?>
<!-- Page wrapper  -->
<!-- ============================================================== -->
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">Data Master</h4>
                <div class="ml-auto text-right">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Library</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!-- End Bread crumb and right sidebar toggle -->
    
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-md-12 col-lg-14 col-xlg-6">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Data Tabel Peminjaman</h5>
                        <div class="table-responsive">
                            <table id="indah" class="table table-striped table-bordered">
                                <button type="button" class="btn btn-outline-warning" style="margin-left: 10px;"><a href="../admin/export_excel_peminjaman.php"><i class="fas fa-file-excel"></i> EXCEL</button>
                                    <button type="button" class="btn btn-outline-warning" style="margin-left: 10px;"><a href="../admin/lap_peminjaman.php"><i class="fas fa-file-pdf"></i> PDF</button>

                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Nama Peminjam</th>
                                                <th>Nama Barang</th>
                                                <th>Tanggal Pinjam</th>
                                                <th>Jumlah Pinjam</th>
                                                <th>Status Peminjaman</th>
                                                <th>Opsi</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                            <?php
                                            $no = 1;
                                            $sql = mysqli_query ($koneksi, "SELECT * FROM peminjaman p JOIN detail_pinjam d ON p.id_peminjaman=d.id_peminjaman JOIN pegawai g ON p.id_pegawai=g.id_pegawai JOIN inventaris i ON d.id_inventaris=i.id_inventaris WHERE status_peminjaman='sedang dipinjam' ");
                                            while ($data = mysqli_fetch_array($sql)) {
                                                ?>
                                                
                                                <tr>
                                                    <td><?php echo $no++; ?></td>
                                                    <td><?php echo $data['nama_pegawai']; ?></td>
                                                    <td><?php echo $data['nama']; ?></td>
                                                    <td><?php echo $data['tanggal_pinjam']; ?></td>
                                                    <td><?php echo $data['jumlah_pinjam']; ?></td>
                                                    <td><?php echo $data['status_peminjaman']; ?></td>
                                                    <?php
                                                    if ($data['status_peminjaman']=='Sedang dipinjam') {
                                                        ?>
                                                        <td>
                                                            <a  href="proses_pengembalianop.php?id_peminjaman=<?=$data['id_peminjaman']?>&id_inventaris=<?=$data['id_inventaris']?>&jumlah=<?=$data['jumlah_pinjam']?>"><button class="btn btn-info">Kembalikan</button></a>
                                                        </td>
                                                        <?php
                                                    }else{
                                                        echo "-";

                                                        ?>
                                                        
                                                        <?php
                                                    }

                                                    ?>
                                                </tr>

                                                <?php
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            
         <!-- ============================================================== -->
     </div>
     <!-- End Page wrapper  -->
 </div>
 
 <?php
 include "footer.php";
 ?>