<?php
include "header.php";
include "../koneksi.php";
?>
<!-- Page wrapper  -->
<!-- ============================================================== -->
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">Form Basic</h4>
                <div class="ml-auto text-right">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Library</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <form method="post" action="protambah_petugas.php" class="form-horizontal">
                        <div class="card-body">
                            <h5 class="card-title m-b-0">Tambah Petugas</h5>
                            <div class="form-group m-t-10">
                                <div class="form-group m-t-20">
                                    <label>Username</label>
                                    <td><input type="text" class="form-control" name="username" placeholder="Masukan Username" required="" autocomplete="off"></td>
                                </div>
                                <div class="form-group m-t-20">
                                    <label>Email</label>
                                    <td><input type="text" class="form-control" name="email" placeholder="Masukan Email" required="" autocomplete="off"></td>
                                </div> 
                                <div class="form-group m-t-20">
                                    <label>Password</label>
                                    <td><input type="text" class="form-control" name="password" placeholder="Masukan Password" required="" autocomplete="off"></td>
                                </div> 
                                <div class="form-group m-t-20">
                                    <label>Nama Petugas</label>
                                    <td><input type="text" class="form-control" name="nama_petugas" placeholder="Masukan Nama Petugas" required="" autocomplete="off"></td>
                                </div> 
                                <div class="form-group m-t-20">
                                    <label>Id Level</label>
                                    <td><input type="text" class="form-control" name="id_level" placeholder="Masukan id_level" required="" autocomplete="off"></td>
                                </div> 
                                
                            </div>
                            <div class="border-top">
                                <div class="card-body">
                                    <button type="submit" name="simpan" value="simpan" class="btn btn-primary">Simpan</button>
                                    <button type="reset" class="btn">Cancel</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <footer class="footer text-center">
                All Rights Reserved by Matrix-admin. Designed and Developed by <a href="https://wrappixel.com">WrapPixel</a>.
            </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <?php
    include "footer.php";
    ?>