n<!DOCTYPE html>
<html>
<head>
  <title>INVENSAPRA SMKN 1 CIOMAS</title>
</head>
<body>
  <style type="text/css">
  body{
    font-family: sans-serif;
  }
  table{
    margin: 20px auto;
    border-collapse: collapse;
  }
  table th,
  table td{
    border: 1px solid #3c3c3c;
    padding: 3px 8px;

  }
  a{
    background: blue;
    color: #fff;
    padding: 8px 10px;
    text-decoration: none;
    border-radius: 2px;
  }
</style>

<?php
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=Data Jenis.xls");
?>

<center>
  <h1>INVENSAPRA SMKN 1 CIOMAS</h1>
</center>

<table border="1">
 <thead>
  <tr>
   <th>#</th>
   <th>Nama Jenis</th>
   <th>Kode Jenis</th>
   <th>Keterangan</th>
 </tr>
</thead>
<tbody>
	<?php
 include "../koneksi.php";
 $no=1;
 $select = mysqli_query ($koneksi,"SELECT * FROM jenis");
 while ($data = mysqli_fetch_array($select)) {
  ?>
  <tr>
    <td><?php echo $no++; ?></td>
    <td><?=$data['nama_jenis']; ?></td>
    <td><?=$data['kode_jenis']; ?></td>
    <td><?=$data['keterangan']; ?></td>		
    <td class="center">
      <a class="btn btn-success" href="editjenis.php?id_jenis=<?=$data['id_jenis'];?>"><i class="mdi mdi-border-color"></i>
      </td>
    </tr>
    <?php
  }
  ?>
</tbody>
</table>

</body>
</html>