<?php
include '../koneksi.php';
include 'pdf/fpdf.php';

$pdf = new FPDF("L","cm","A4");

$pdf->SetMargins(1,1,1);
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Times','B',11);

$pdf->SetX(4);            
$pdf->MultiCell(19.5,0.5,'SMK NEGERI 1 CIOMAS',0,'C');
$pdf->SetX(4);
$pdf->MultiCell(19.5,0.5,'TEKNOLOGI DAN REKAYASA',0,'C');
$pdf->SetX(4);
$pdf->MultiCell(19.5,0.5,'TEKNOLOGI INFORMASI DAN KOMUNIKASI',0,'C');
$pdf->SetX(4);
$pdf->MultiCell(19.5,0.5,'NSS:401020229101     NPSN:20254135',0,'C');
$pdf->SetX(4);
$pdf->MultiCell(19.5,0.5,'Jl.Raya Laladon Ds.Laladon, Kec.Ciomas Kab.Bogor Telp.(0251) 8631261 Kode Pos.16610',0,'C');
$pdf->SetX(4);
$pdf->MultiCell(19.5,0.5,'Email : smkn1_ciomas@yahoo,co.id, Web : www.smkn1ciomas.sch.id',0,'C');
$pdf->Line(1,4.1,28.7,4.1);
$pdf->SetLineWidth(0.1);      
$pdf->Line(1,4.2,28.5,4.2);   
$pdf->SetLineWidth(0);
$pdf->ln(1);
$pdf->SetFont('Arial','B',14);
$pdf->Cell(25.5,0.7,"Laporan Data Inventaris",0,10,'C');
$pdf->ln(1);
$pdf->SetFont('Arial','B',9);
$pdf->Cell(5,0.7,"Di cetak pada : ".date("D-d/m/Y"),0,0,'C');
$pdf->ln(1);

$pdf->SetFont('Arial','B',10);
$pdf->Cell(1, 0.8, 'NO', 1, 0, 'C');
$pdf->Cell(5, 0.8, 'Nama Ruang', 1, 0, 'C');
$pdf->Cell(3, 0.8, 'Kode Ruang', 1, 0, 'C');
$pdf->Cell(5, 0.8, 'keterangan', 1, 1, 'C');
$pdf->SetFont('Arial','',10);
$no=1;
$query=mysqli_query ($koneksi,"SELECT * FROM ruang");
while ($lihat = mysqli_fetch_array($query)){
	$pdf->Cell(1, 0.8, $no , 1, 0, 'C');
	$pdf->Cell(5, 0.8, $lihat['nama_ruang'],1, 0, 'C');
	$pdf->Cell(3, 0.8, $lihat['kode_ruang'], 1, 0,'C');
	$pdf->Cell(5., 0.8, $lihat['keterangan'],1, 1, 'C');

	$no++;
}

$pdf->Output("laporan_ruang.pdf","I");

?>


