<?php
include "header.php";
?>
<!-- Page wrapper  -->
<!-- ============================================================== -->
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">Data Master</h4>
                <div class="ml-auto text-right">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Library</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!-- End Bread crumb and right sidebar toggle -->
    
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-md-12 col-lg-14 col-xlg-6">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Basic Datatable</h5>
                        <div class="table-responsive">
                            <table id="indah" class="table table-striped table-bordered">
                               <button type="button" class="btn btn-outline-warning" style="margin-left: 10px;"><a href="tambahruang.php"><i class="fas fa-plus-circle"></i> ADD</button>
                                <button type="button" class="btn btn-outline-warning" style="margin-left: 10px;"><a href="export_excel_ruang.php"><i class="fas fa-file-excel"></i> EXCEL
                                </button>
                                <button type="button" class="btn btn-outline-warning" style="margin-left: 10px;"><a href="lap_ruang.php"><i class="fas fa-file-pdf"></i> PDF
                                </button>                                    
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Nama Ruang</th>
                                        <th>Kode Ruang</th>
                                        <th>Keterangan</th>
                                        <th>Opsi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                  <?php
                                  include "../koneksi.php";
                                  $no=1;
                                  $select = mysqli_query ($koneksi,"SELECT * FROM ruang");
                                  while ($data = mysqli_fetch_array($select)) {
                                    ?>
                                    <tr>
                                        <td><?php echo $no++; ?></td>
                                        <td><?=$data['nama_ruang']; ?></td>
                                        <td><?=$data['kode_ruang']; ?></td>
                                        <td><?=$data['keterangan']; ?></td>		
                                        <td class="center">
                                            <a class="btn btn-circle btn-lg btn-cyan" href="editruang.php?id_ruang=<?=$data['id_ruang'];?>"><i class="mdi mdi-border-color"></i>
                                            <a>
                                            <a class="btn btn-circle btn-lg btn-danger" href="hapusruang.php?id_ruang=<?=$data['id_ruang'];?>"><i class="mdi mdi-delete"></i></a>
                                            </td>
                                        </tr>

                                        <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Right sidebar -->
        <!-- ============================================================== -->
        <!-- .right-sidebar -->
        <!-- ============================================================== -->
        <!-- End Right sidebar -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- footer -->
    <!-- ============================================================== -->
   
    <!-- ============================================================== -->
    <!-- End footer -->
    <!-- ============================================================== -->
</div>
<!-- End Page wrapper  -->
</div>

<?php
include "footer.php";
?>