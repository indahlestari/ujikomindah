<?php
include "header.php";
include "../koneksi.php";
$id_ruang=$_GET['id_ruang'];
$ruang= mysqli_query($koneksi, "SELECT * FROM ruang WHERE id_ruang='$id_ruang'");
$r = mysqli_fetch_array($ruang)
?>
<!-- Page wrapper  -->
<!-- ============================================================== -->
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">Form Basic</h4>
                <div class="ml-auto text-right">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Library</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <form action="" method="post" class="form-horizontal">
                        <div class="card-body">
                            <h5 class="card-title m-b-0">Edit Ruang</h5>
                            <div class="form-group m-t-20">
                                <label for="nama_ruang">Nama Ruang</label>
                                <input type="text" name="nama_ruang" class="form-control date-inputmask" id="nama_ruang" placeholder="" autocomplete="off" required="required" value="<?=$r['nama_ruang'];?>">
                            </div>
                            <div class="form-group m-t-20">
                                <label for="kode_ruang">Kode Ruang</label>
                                <input type="text" name="kode_ruang" class="form-control date-inputmask" id="kode_ruang" placeholder="" autocomplete="off" required="required" value="<?=$r['kode_ruang'];?>" readonly>
                            </div>
                            <div class="form-group m-t-20">
                                <label for="keterangan">Keterangan</label>
                                <input type="text" name="keterangan" class="form-control date-inputmask" id="keterangan" placeholder="" autocomplete="off" required="required" value="<?=$r['keterangan'];?>">
                            </div>

                            <div class="border-top">
                                <div class="card-body">
                                    <button type="submit" name="edit" class="btn btn-primary">Submit</button>
                                </div>
                            </div>
                        </form>
                        <?php
                        if (isset($_POST['edit'])){

                            $nama_ruang = $_POST['nama_ruang'];
                            $kode_ruang = $_POST['kode_ruang'];
                            $keterangan = $_POST['keterangan'];

                            $sql = mysqli_query($koneksi, "UPDATE ruang SET nama_ruang='$nama_ruang', kode_ruang='$kode_ruang', keterangan='$keterangan' WHERE id_ruang='$id_ruang'");

                            if (sql){
                             echo "<script>
                             window.alert('Data Berhasil Diedit')
                             window.location='ruang.php'
                             </script>";
                         } else {
                            echo "Gagal Disimpan";
                        }
                    }
                    ?>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Container fluid  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- footer -->
    </div>
    <!-- ============================================================== -->
    <!-- End Page wrapper  -->
    <!-- ============================================================== -->
</div>
<?php
include "footer.php";
?>