<?php
include "header.php";
include "../koneksi.php";
?>
<!-- Page wrapper  -->
<!-- ============================================================== -->
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">Form Basic</h4>
                <div class="ml-auto text-right">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Library</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <form action="protambah_ruang.php" method="post" class="form-horizontal">
                        <div class="card-body">
                            <h5 class="card-title m-b-0">Tambah Ruang</h5>
                            <div class="form-group m-t-10">
                                <div class="form-group m-t-20">
                                    <label for="nama_ruang">Nama Ruang</label>
                                    <input type="text" class="form-control date-inputmask" name="nama_ruang" id="nama_ruang" placeholder="Masukan Nama Ruang" required="">
                                </div>
                                
                                <div class="form-group m-t-20">
                                    <label for="kode_ruang">Kode Jenis Barang</label>
                                    <div>
                                        <?php
                                        $koneksi = mysqli_connect("localhost","root","","ujikom_indah");
                                        $carikode = mysqli_query($koneksi, "SELECT max(kode_ruang) as kode from ruang");
                                                    //Max atau kode yang baru masuk
                                        $tm_cari = mysqli_fetch_array($carikode);
                                        $kode    = substr($tm_cari['kode'],1,4);
                                        $tambah  = $kode+1;
                                        if ($tambah<10){
                                            $kode_ruang = "R000".$tambah;
                                        } else {
                                            $kode_ruang = "R00".$tambah;
                                        }
                                        ?>
                                        <input type="text" class="form-control date-inputmask" name="kode_ruang" id="kode_ruang" value="<?php echo $kode_ruang; ?>" placeholder="" required="" readonly="">
                                    </select>
                                </div>
                            </div>
                            <div class="form-group m-t-20">
                                <label for="keterangan">Keterangan</label>
                                <input type="text" class="form-control date-inputmask" name="keterangan" id="keterangan" placeholder="Masukan Keterangan Barang" required="">
                            </div>
                        </div>
                        <div class="border-top">
                            <div class="card-body">
                                <button type="submit" name="simpan" value="simpan" class="btn btn-primary">Simpan</button>
                                <button type="reset" class="btn">Cancel</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Container fluid  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- footer -->
        <!-- ============================================================== -->
       
        <!-- ============================================================== -->
        <!-- End footer -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Page wrapper  -->
    <!-- ============================================================== -->
</div>
<?php
include "footer.php";
?>