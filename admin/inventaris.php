<?php
include "header.php";
?>
<!-- Page wrapper  -->
<!-- ============================================================== -->
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">Data Master</h4>
                <div class="ml-auto text-right">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Library</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!-- End Bread crumb and right sidebar toggle -->
    
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-md-12 col-lg-14 col-xlg-6">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Basic Datatable</h5>
                        <div class="table-responsive">
                            <table id="indah" class="table table-striped table-bordered">
                                <button type="button" class="btn btn-outline-warning" style="margin-left: 10px;"><a href="tambahinventaris.php"><i class="fas fa-plus-circle"></i> ADD</button>
                                    <button type="button" class="btn btn-outline-warning" style="margin-left: 10px;"><a href="export_excel_inventaris.php"><i class="fas fa-file-excel"></i> EXCEL</button>
                                        <button type="button" class="btn btn-outline-warning" style="margin-left: 10px;"><a href="lap_barang.php"><i class="fas fa-file-pdf"></i> PDF</button>
                                         
                                        </tr>
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Nama Barang</th>
                                                <th>Kondisi</th>
                                                <th>Keterangan</th>
                                                <th>Jumlah</th>
                                                <th>Nama Jenis</th>
                                                <th>Tanggal register</th>
                                                <th>Nama Ruang</th>
                                                <th>Kode Inventaris</th>
                                                <th>Nama Petugas</th>
                                                <th>Gambar</th>
                                                <th>Sumber</th>
                                                <th>Opsi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            include "../koneksi.php";
                                            if (isset($_GET['jurusan'])){
                                                $bebas = $_GET['jurusan'];
                                                $pilih = mysqli_query($koneksi,"SELECT * FROM inventaris i JOIN jenis j ON i.id_jenis=j.id_jenis JOIN ruang r ON i.id_ruang=r.id_ruang JOIN petugas p ON i.id_petugas=p.id_petugas WHERE nama_jenis='$bebas'");
                                            }
                                            else {
                                                $pilih = mysqli_query($koneksi,"SELECT * FROM inventaris");
                                            }
                                            $no=1;
                                            while ($data = mysqli_fetch_array($pilih)) {
                                                ?>
                                                <tr>
                                                    <td><?php echo $no++; ?></td>
                                                    <td><?php echo $data['nama']; ?></td>
                                                    <td><?php echo $data['kondisi']; ?></td>
                                                    <td><?php echo $data['keterangan']; ?></td>
                                                    <td><?php echo $data['jumlah']; ?></td>
                                                    <td><?php echo $data['nama_jenis']; ?></td>
                                                    <td><?php echo $data['tanggal_register']; ?></td>
                                                    <td><?php echo $data['nama_ruang']; ?></td>
                                                    <td><?php echo $data['kode_inventaris']; ?></td>
                                                    <td><?php echo $data['nama_petugas']; ?></td>  
                                                    <td><?php echo $data['sumber']; ?></td>        
                                                    <td><img src="../assets/images/barang/<?=$data['foto'];?>" width="50px"></td>
                                                    <td class="center">
                                                        <a class="btn btn-circle btn-lg btn-cyan" href="editinven.php?id_inventaris=<?=$data['id_inventaris'];?>"><i class="mdi mdi-border-color"></i>
                                                        </a>
                                                        <a class="btn btn-circle btn-lg btn-danger" href="hapusinven.php?id_inventaris=<?=$data['id_inventaris'];?>"><i class="mdi mdi-delete"></i>
                                                        </a>
                                                    </td>
                                                </tr>

                                                <?php
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
        </div>
        <!-- End Page wrapper  -->
    </div>
    
    <?php
    include "footer.php";
    ?>

