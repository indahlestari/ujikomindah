<?php
include "header.php";
include "../koneksi.php";
?>
<!-- Page wrapper  -->
<!-- ============================================================== -->
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">Form Basic</h4>
                <div class="ml-auto text-right">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Library</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <form action="protambah_pegawai.php" method="post" class="form-horizontal">
                        <div class="card-body">
                            <h5 class="card-title m-b-0">Tambah Pegawai</h5>
                            <div class="form-group m-t-10">
                                <div class="form-group m-t-20">
                                    <label for="nama_ruang">Nama Pegawai</label>
                                    <input type="text" class="form-control date-inputmask" name="nama_pegawai" id="nama_pegawai" placeholder="Masukan Nama pegawai" required="">
                                </div>
                                <div class="form-group m-t-20">
                                    <label for="keterangan">Nip</label>
                                    <input type="text" class="form-control date-inputmask" name="nip" id="nip" placeholder="Masukan Nip" required="">
                                </div>
                                <div class="form-group m-t-20">
                                    <label for="keterangan">Alamat</label>
                                    <input type="text" class="form-control date-inputmask" name="alamat" id="alamat" placeholder="Masukan Alamat" required="">
                                </div>
                                <div class="form-group m-t-20">
                                    <label for="keterangan">No Telepon</label>
                                    <input type="text" class="form-control date-inputmask" name="no_telfon" id="no_telfon" placeholder="Masukan No Telepon" required="">
                                </div>
                            </div>
                            <div class="border-top">
                                <div class="card-body">
                                    <button type="submit" name="simpan" value="simpan" class="btn btn-primary">Simpan</button>
                                    <button type="reset" class="btn">Cancel</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <footer class="footer text-center">
                All Rights Reserved by Matrix-admin. Designed and Developed by <a href="https://wrappixel.com">WrapPixel</a>.
            </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <?php
    include "footer.php";
    ?>