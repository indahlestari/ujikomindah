<?php
include "header.php";
include "../koneksi.php";
$id_petugas=$_GET['id_petugas'];
$petugas= mysqli_query($koneksi, "SELECT * FROM petugas WHERE id_petugas='$id_petugas'");
$r = mysqli_fetch_array($petugas)
?>
<!-- Page wrapper  -->
<!-- ============================================================== -->
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">Form Basic</h4>
                <div class="ml-auto text-right">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Library</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <form action="" method="post" class="form-horizontal">
                        <div class="card-body">
                            <h5 class="card-title m-b-0">Edit Petugas</h5>
                            <div class="form-group m-t-20">
                                <label for="username">Username</label>
                                <input type="text" name="username" class="form-control date-inputmask" id="username" placeholder="" autocomplete="off" required="required" value="<?=$r['username'];?>">
                            </div>
                            <div class="form-group m-t-20">
                                <label for="email">Email</label>
                                <input type="text" name="email" class="form-control date-inputmask" id="email" placeholder="" autocomplete="off" required="required" value="<?=$r['email'];?>">
                            </div>
                            <div class="form-group m-t-20">
                                <label for="password">Password</label>
                                <input type="text" name="password" class="form-control date-inputmask" id="password" placeholder="" autocomplete="off" required="required" value="<?=$r['password'];?>">
                            </div>
                            <div class="form-group m-t-20">
                                <label for="nama_petugas">Nama Petugas</label>
                                <input type="text" name="nama_petugas" class="form-control date-inputmask" id="nama_petugas" placeholder="" autocomplete="off" required="required" value="<?=$r['nama_petugas'];?>">
                            </div>
                            <div class="form-group m-t-20">
                                <label for="id_level">Id Level</label>
                                <input type="text" name="id_level" class="form-control date-inputmask" id="id_level" placeholder="" autocomplete="off" required="required" value="<?=$r['id_level'];?>">
                            </div>

                            <div class="border-top">
                                <div class="card-body">
                                    <button type="submit" name="edit" class="btn btn-primary">Submit</button>
                                </div>
                            </div>
                        </form>
                        <?php
                        if (isset($_POST['edit'])){

                            $username = $_POST['username'];
                            $email = $_POST['email'];
                            $password = md5($_POST['password']);
                            $nama_petugas = $_POST['nama_petugas'];
                            $id_level = $_POST['id_level'];

                            $sql = mysqli_query($koneksi, "UPDATE petugas SET username='$username', email='$email', password='$password', nama_petugas='$nama_petugas', id_level='$id_level' WHERE id_petugas='$id_petugas'");

                            if (sql){
                               echo "<script>
                               window.alert('Data Berhasil Diedit')
                               window.location='petugas.php'
                               </script>";
                           } else {
                            echo "Gagal Disimpan";
                        }
                    }
                    ?>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Container fluid  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- footer -->
    </div>
    <!-- ============================================================== -->
    <!-- End Page wrapper  -->
    <!-- ============================================================== -->
</div>
<?php
include "footer.php";
?>