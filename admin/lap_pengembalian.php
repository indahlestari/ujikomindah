<?php
include '../koneksi.php';
include 'pdf/fpdf.php';

$pdf = new FPDF("L","cm","A4");

$pdf->SetMargins(1,1,1);
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Times','B',11);

$pdf->SetX(4);            
$pdf->MultiCell(19.5,0.5,'SMK NEGERI 1 CIOMAS',0,'C');
$pdf->SetX(4);
$pdf->MultiCell(19.5,0.5,'TEKNOLOGI DAN REKAYASA',0,'C');
$pdf->SetX(4);
$pdf->MultiCell(19.5,0.5,'TEKNOLOGI INFORMASI DAN KOMUNIKASI',0,'C');
$pdf->SetX(4);
$pdf->MultiCell(19.5,0.5,'NSS:401020229101     NPSN:20254135',0,'C');
$pdf->SetX(4);
$pdf->MultiCell(19.5,0.5,'Jl.Raya Laladon Ds.Laladon, Kec.Ciomas Kab.Bogor Telp.(0251) 8631261 Kode Pos.16610',0,'C');
$pdf->SetX(4);
$pdf->MultiCell(19.5,0.5,'Email : smkn1_ciomas@yahoo,co.id, Web : www.smkn1ciomas.sch.id',0,'C');
$pdf->Line(1,4.1,28.7,4.1);
$pdf->SetLineWidth(0.1);      
$pdf->Line(1,4.2,28.5,4.2);   
$pdf->SetLineWidth(0);
$pdf->ln(1);
$pdf->SetFont('Arial','B',14);
$pdf->Cell(25.5,0.7,"Laporan Data Peminjaman",0,10,'C');
$pdf->ln(1);
$pdf->SetFont('Arial','B',9);
$pdf->Cell(5,0.7,"Di cetak pada : ".date("D-d/m/Y"),0,0,'C');
$pdf->ln(1);

$pdf->SetFont('Arial','B',10);
$pdf->Cell(1, 0.8, 'NO', 1, 0, 'C');
$pdf->Cell(6, 0.8, 'Nama Peminjam', 1, 0, 'L');
$pdf->Cell(3, 0.8, 'Nama Barang', 1, 0, 'C');
$pdf->Cell(5, 0.8, 'Tanggal Peminjaman', 1, 0, 'C');
$pdf->Cell(5, 0.8, 'Tanggal Kembali', 1, 0, 'C');
$pdf->Cell(3, 0.8, 'Jumlah', 1, 0, 'C');
$pdf->Cell(5, 0.8, 'Status Peminjaman', 1, 1, 'C');
$pdf->SetFont('Arial','',10);
$no=1;
$query=mysqli_query ($koneksi,"SELECT * FROM peminjaman p JOIN detail_pinjam d ON p.id_peminjaman=d.id_peminjaman JOIN pegawai g ON p.id_pegawai=g.id_pegawai JOIN inventaris i ON d.id_inventaris=i.id_inventaris where status_peminjaman='Telah Dikembalikan'");
while ($lihat = mysqli_fetch_array($query)){
	$pdf->Cell(1, 0.8, $no , 1, 0, 'C');
	$pdf->Cell(6, 0.8, $lihat['nama_pegawai'],1, 0, 'L');
	$pdf->Cell(3, 0.8, $lihat['nama'], 1, 0,'C');
	$pdf->Cell(5, 0.8, $lihat['tanggal_pinjam'], 1, 0,'C');
	$pdf->Cell(5, 0.8, $lihat['tanggal_kembali'], 1, 0,'C');
	$pdf->Cell(3, 0.8, $lihat['jumlah_pinjam'], 1, 0,'C');
	$pdf->Cell(5., 0.8, $lihat['status_peminjaman'],1, 1, 'C');

	$no++;
}

$pdf->Output("laporan_peminjaman.pdf","I");

?>


