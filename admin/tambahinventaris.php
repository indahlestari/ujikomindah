<?php
include "header.php";
include "../koneksi.php";
?>
<!-- Page wrapper  -->
<!-- ============================================================== -->
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">Form Basic</h4>
                <div class="ml-auto text-right">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Library</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <form action="protambah_inven.php" method="post" class="form-horizontal">
                    <div class="card-body">
                        <h5 class="card-title m-b-0">Tambah Inventaris</h5>
                        <div class="form-group m-t-10">
                                                <div class="col-sm-offset-1 col-sm-10">
                          <div class="form-group">
                            <label>Nama Barang</label>
                            <td><input type="text"  class="form-control"  name="nama" autocomplete="off" required></td>
                        </div>
                        <label>Kondisi</label><br>
                        <select class="form-control" name="kondisi">
                            <option disabled selected>Pilih Kondisi</option>
                            <option>Baik</option>
                            <option>Rusak</option>
                            <option>Rusak Ringan</option>
                        </select><br>
                        <div class="form-group">
                            <label>Keterangan</label>
                            <td><input type="text"  class="form-control"  name="keterangan" autocomplete="off" required></td>
                        </div>
                        <div class="form-group">
                            <label>Jumlah</label>
                            <td><input type="number"  class="form-control"  name="jumlah" autocomplete="off" required></td>
                        </div>
                        <div class="form-group">
                            <label for="id_jenis">Nama Jenis</label>
                            <select class="form-control" name="id_jenis">
                              <option value="" disabled selected>Pilih Nama Jenis</option>
                              <?php
                              include '../koneksi.php';
                              $query = mysqli_query($koneksi, "SELECT * FROM jenis");
                              while($data=mysqli_fetch_array($query)) {
                                ?>
                                <option value="<?php echo $data['id_jenis'];?>"><?php echo $data['nama_jenis'];?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="form-group">
                          <label>Tanggal Register</label>
                          <td><input type="date"  class="form-control"  name="tanggal_register" autocomplete="off" required></td>
                      </div>
                      <div class="form-group">
                          <label for="id_ruang">Nama Ruang</label>
                          <select class="form-control" name="id_ruang">
                            <option value="" disabled selected>Pilih Ruang</option>
                            <?php
                            include '../koneksi.php';
                            $select = mysqli_query($koneksi, "SELECT * FROM ruang");
                            while($isi=mysqli_fetch_array($select)) {
                              ?>
                              <option value="<?php echo $isi['id_ruang'];?>"><?php echo $isi['nama_ruang'];?></option>
                              <?php } ?>
                          </select>
                      </div>
                      <div class="form-group">
                        <label>Kode Inventaris</label>
                        <?php
                        $koneksi = mysqli_connect("localhost","root","","ujikom_indah");
                        $cari_kd = mysqli_query($koneksi, "SELECT max(kode_inventaris) as kode from inventaris");
                                        // besar / kode yang masuk
                        $tm_cari = mysqli_fetch_array($cari_kd);
                        $kode = substr($tm_cari['kode'],1,4);
                        $tambah = $kode+1;
                        if ($tambah<10) {
                          $kode_inventaris = "V000".$tambah;
                      }else{
                          $kode_inventaris = "V00".$tambah;
                      }
                      ?>
                      <input class="form-control" name="kode_inventaris" value="<?php echo $kode_inventaris; ?>" type="text" placeholder="Masukan Kode Inventaris" required readonly>
                  </div>

                                  <!--  <div class="form-group">
                                        <label for="id_petugas">Petugas</label>
                                        <input type="text" class="form-control" value="<?php echo $_SESSION['username'] ?>" readonly>
                                        <input type="hidden" class="form-control" value="<?php echo $_SESSION['id_pengguna'] ?>" name="id_petugas" readonly>
                                    </div>  -->  
                                    <div class="form-group">
                                        <label for="id_petugas">Petugas</label>
                                        <select class="form-control" name="id_petugas" readonly>
                                           
                                          <?php
                                          include '../koneksi.php';
                                          $select = mysqli_query($koneksi, "SELECT * FROM petugas");
                                          while($isi=mysqli_fetch_array($select)) {
                                            ?>
                                            <option value="<?php echo $isi['id_petugas'];?>"><?php echo $isi['nama_petugas'];?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Sumber</label>
                                        <td><input type="text"  class="form-control"  name="sumber" autocomplete="off" required></td>
                                    </div>
                                    <div class="form-group">
                                      <label for="exampleInputFile">File input</label>
                                      <input type="file" name="foto" id="exampleInputFile" placeholder="foto">
                                      <p class="help-block">Masukan Gambar Inventaris.</p>
                                  </div>
                                  <div class="border-top">
                            <div class="card-body">
                                <button type="submit" name="simpan" value="simpan" class="btn btn-primary">Simpan</button>
                                <button type="reset" class="btn">Cancel</button>
                            </div>
                        </div>
                        </div>
                       
                        </form>

                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
             </div>
    <!-- ============================================================== -->
    <!-- End Page wrapper  -->
    <!-- ============================================================== -->
</div>
<?php
include "footer.php";
?>