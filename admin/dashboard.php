<?php
include 'header.php';
?>
<!-- Page wrapper  -->
<!-- ============================================================== -->
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">Dashboard</h4>
                <div class="ml-auto text-right">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Library</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- Cards -->
        <div class="card">
            <div class="card-body">
                <div class="alert alert-danger" role="alert">
                  Hallo Admin !
              </div>

          </div>

      </div>
      <div class="row">

        <div class="col-md-6 col-lg-3 col-xlg-3">
        <div class="card card-hover">
            <div class="box bg-danger text-center">
                <h1 class="font-light text-white"><i class="mdi mdi-border-outside"></i></h1>
                <?php
                require '../koneksi.php';
                $query = mysqli_query($koneksi,"SELECT count(id_pegawai) as c FROM pegawai");
                $data = mysqli_fetch_assoc($query);
                ?>
                <h5 class="text-white"><?php echo $data['c']; ?>  Pegawai</h5>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-lg-3 col-xlg-3">
        <div class="card card-hover">
            <div class="box bg-warning text-center">
                <h1 class="font-light text-white"><i class="mdi mdi-collage"></i></h1>
                <?php
                require '../koneksi.php';
                $query = mysqli_query($koneksi,"SELECT count(id_ruang) as b FROM ruang");
                $data = mysqli_fetch_assoc($query);
                ?>
                <h5 class="text-white"><?php echo $data['b']; ?>  Ruang</h5>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-lg-3 col-xlg-3">
        <div class="card card-hover">
            <div class="box bg-success text-center">
                <h1 class="font-light text-white"><i class="mdi mdi-calendar-check"></i></h1>
                <?php
                require '../koneksi.php';
                $query = mysqli_query($koneksi,"SELECT count(id_petugas) as d FROM petugas");
                $data = mysqli_fetch_assoc($query);
                ?>
                <h5 class="text-white"><?php echo $data['d']; ?>  Petugas</h5>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-lg-3 col-xlg-3">
        <div class="card card-hover">
            <div class="box bg-cyan text-center">
                <h1 class="font-light text-white"><i class="mdi mdi-view-dashboard"></i></h1>
                <?php
                require '../koneksi.php';
                $query = mysqli_query($koneksi,"SELECT count(id_inventaris) as a FROM inventaris");
                $data = mysqli_fetch_assoc($query);
                ?>
                <h5 class="text-white"><?php echo $data['a']; ?>  Barang</h5>
            </div>
        </div>
    </div>
</div>
</div>
<!-- End cards -->
</div>
</div>
<!-- ============================================================== -->
<?php
include 'footer.php';
?>