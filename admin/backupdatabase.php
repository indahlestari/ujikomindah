<?php
include 'header.php';
?>
<!-- Page wrapper  -->
<!-- ============================================================== -->
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">Backup Database</h4>
                <div class="ml-auto text-right">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Library</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">

        <!-- Charts -->
        <div class="row">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-body">
                        <h3 class="card-title">Backup Database</h3>
                        <div style="height: 400px;"><br>
                            <h5>Data kamu sudah penuh!! saatnya Backup Database</h5>
                            <form action="prosesbackupdb.php" method="post" class="form-horizontal">
                                <div class="border-top">
                                    <div class="card-body">
                                        <button type="submit" name="simpan" value="simpan" class="btn btn-primary">Backup</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card">
                    <div class="card-body">
                        <h3 class="card-title" style="text-align: center;">Pilih Tabel Untuk Export</h3>
                        <div class="table-responsive">
                            <div class="checkbox" style="height: 400px;">
                             <table class="table">
                                <thead class="thead-light">
                                    <tr>
                                        <th scope="col">No</th>
                                        <th scope="col">Table</th>
                                        <th scope="col">Opsi</th>

                                    </tr>
                                </thead>
                                <tbody class="customtable">
                                    <form action="prosesbackupdetail.php" method="post" class="form-horizontal">
                                        <tr>
                                            <td>1</td>
                                            <td>Detail Pinjam</td>
                                        <td>
                                            <button type="submit" name="simpan" value="simpan" class="btn btn-primary">Backup</button>
                                        </td>
                                        </tr>
                                    </form>
                                    </tr>
                                    <form action="prosesbackupinventaris.php" method="post" class="form-horizontal">
                                        <tr>
                                            <td>2</td>
                                            <td>Inventaris</td>
                                        <td>
                                                <button type="submit" name="simpan" value="simpan" class="btn btn-primary">Backup</button>
                                        </td>
                                        </tr>
                                    </form>
                                    <form action="prosesbackupjenis.php" method="post" class="form-horizontal">
                                        <tr>
                                            <td>3</td>
                                            <td>Jenis</td>
                                        <td>
                                            <button type="submit" name="simpan" value="simpan" class="btn btn-primary">Backup</button>
                                        </td>
                                        </tr>
                                    </form>
                                    <form action="prosesbackuplevel.php" method="post" class="form-horizontal">
                                        <tr>    
                                            <td>4</td>
                                            <td>Level</td>
                                        <td>
                                            <button type="submit" name="simpan" value="simpan" class="btn btn-primary">Backup</button>
                                        </td>
                                        </tr>
                                    </form>
                                    <form action="prosesbackuppegawai.php" method="post" class="form-horizontal">
                                        <tr>
                                            <td>5</td>
                                            <td>Pegawai</td>
                                        <td>
                                            <button type="submit" name="simpan" value="simpan" class="btn btn-primary">Backup</button>
                                        </td>
                                        </tr>
                                    </form>
                                    <form action="prosesbackuppeminjaman.php" method="post" class="form-horizontal">
                                        <tr>
                                            <td>6</td>
                                            <td>Peminjaman</td>
                                        <td>
                                            <button type="submit" name="simpan" value="simpan" class="btn btn-primary">Backup</button>
                                        </td>
                                        </tr>
                                    </form>
                                    <form action="prosesbackuppetugas.php" method="post" class="form-horizontal">
                                        <tr>
                                            <td>7</td>
                                            <td>Petugas</td>
                                        <td>
                                           <button type="submit" name="simpan" value="simpan" class="btn btn-primary">Backup</button>
                                        </td>
                                        </tr>
                                    </form>
                                    <form action="prosesbackupruang.php" method="post" class="form-horizontal">
                                        <tr>
                                            <td>8</td>
                                            <td>Ruang</td>
                                        <td>
                                            <button type="submit" name="simpan" value="simpan" class="btn btn-primary">Backup</button>
                                        </td>
                                        </tr>
                                    </form>
                                        </tr>
                                </tbody>
                                </table>
                                </div>
                            </div>
                        </div> 
                    </div>
                </div>
            </div>
        </div>
      </div>
    </div>
</div>

<!-- ============================================================== -->
    <?php
    include 'footer.php';
    ?>