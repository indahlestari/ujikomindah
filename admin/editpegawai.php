<?php
include "header.php";
include "../koneksi.php";
$id_pegawai=$_GET['id_pegawai'];
$pegawai= mysqli_query($koneksi, "SELECT * FROM pegawai WHERE id_pegawai='$id_pegawai'");
$r = mysqli_fetch_array($pegawai)
?>
<!-- Page wrapper  -->
<!-- ============================================================== -->
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">Form Basic</h4>
                <div class="ml-auto text-right">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Library</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <form action="" method="post" class="form-horizontal">
                        <div class="card-body">
                            <h5 class="card-title m-b-0">Edit Pegawai</h5>
                            <div class="form-group m-t-20">
                                <label for="nama_pegawai">Nama Pegawai</label>
                                <input type="text" name="nama_pegawai" class="form-control date-inputmask" id="nama_pegawai" placeholder="" autocomplete="off" required="required" value="<?=$r['nama_pegawai'];?>">
                            </div>
                            <div class="form-group m-t-20">
                                <label for="nip">Nip</label>
                                <input type="text" name="nip" class="form-control date-inputmask" id="nip" placeholder="" autocomplete="off" required="required" value="<?=$r['nip'];?>">
                            </div>
                            <div class="form-group m-t-20">
                                <label for="alamat">Alamat</label>
                                <input type="text" name="alamat" class="form-control date-inputmask" id="alamat" placeholder="" autocomplete="off" required="required" value="<?=$r['alamat'];?>">
                            </div>
                            <div class="form-group m-t-20">
                                <label for="no_telfon">No Telpon</label>
                                <input type="text" name="no_telfon" class="form-control date-inputmask" id="no_telfon" placeholder="" autocomplete="off" required="required" value="<?=$r['no_telfon'];?>">
                            </div>

                            <div class="border-top">
                                <div class="card-body">
                                    <button type="submit" name="edit" class="btn btn-primary">Submit</button>
                                </div>
                            </div>
                        </form>
                        <?php
                        if (isset($_POST['edit'])){

                         $nama_pegawai   =$_POST['nama_pegawai'];
                         $nip            =$_POST['nip'];
                         $alamat         =$_POST['alamat'];
                         $no_telfon      =$_POST['no_telfon'];

                         $sql = mysqli_query($koneksi, "UPDATE pegawai SET nama_pegawai='$nama_pegawai', nip='$nip', alamat='$alamat', no_telfon='$no_telfon' WHERE id_pegawai='$id_pegawai'");

                         if (sql){
                             echo "<script>
                             window.alert('Data Berhasil Diedit')
                             window.location='pegawai.php'
                             </script>";
                         } else {
                            echo "Gagal Disimpan";
                        }
                    }
                    ?>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Container fluid  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- footer -->
    </div>
    <!-- ============================================================== -->
    <!-- End Page wrapper  -->
    <!-- ============================================================== -->
</div>
<?php
include "footer.php";
?>