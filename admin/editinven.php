<?php
include "header.php";
include "../koneksi.php";
$id_inventaris=$_GET['id_inventaris'];
$inventaris= mysqli_query($koneksi, "SELECT * FROM inventaris WHERE id_inventaris='$id_inventaris'");
$r = mysqli_fetch_array($inventaris)
?>
<!-- Page wrapper  -->
<!-- ============================================================== -->
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">Form Basic</h4>
                <div class="ml-auto text-right">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Library</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <form action="" method="post" class="form-horizontal">
                        <div class="card-body">
                            <h5 class="card-title m-b-0">Edit Inventaris</h5>
                            <div class="form-group m-t-20">
                                <label for="nama">Nama Barang</label>
                                <input type="text" name="nama" class="form-control date-inputmask" id="nama" placeholder="" autocomplete="off" required="required" value="<?=$r['nama'];?>">
                            </div>
                            <div class="form-group m-t-20">
                                <label for="kondisi">Kondisi Barang</label>
                                <input type="text" name="kondisi" class="form-control date-inputmask" id="kondisi" placeholder="" autocomplete="off" required="required" value="<?=$r['kondisi'];?>">
                            </div>
                            <div class="form-group m-t-20">
                                <label for="jumlah">Jumlah</label>
                                <input type="text" name="jumlah" class="form-control date-inputmask" id="jumlah" placeholder="" autocomplete="off" required="required" value="<?=$r['jumlah'];?>">
                            </div>
                            <div class="form-group m-t-20">
                                <label for="nama_jenis">Id Jenis</label>
                                <input type="text" name="nama_jenis" class="form-control date-inputmask" id="nama_jenis" placeholder="" autocomplete="off" required="required" value="<?=$r['nama_jenis'];?>" readonly>
                            </div>
                            <div class="form-group m-t-20">
                                <label for="tanggal_register">Tanggal Register</label>
                                <input type="date" name="tanggal_register" class="form-control date-inputmask" id="tanggal_register" placeholder="" autocomplete="off" required="required" value="<?=$r['tanggal_register'];?>">
                            </div>
                            <div class="form-group m-t-20">
                                <label for="id_ruang">Id Ruang</label>
                                <input type="text" name="id_ruang" class="form-control date-inputmask" id="id_ruang" placeholder="" autocomplete="off" required="required" value="<?=$r['id_ruang'];?>" readonly>
                            </div>
                            <div class="form-group m-t-20">
                                <label for="kode_inventaris">Kode Inventaris</label>
                                <input type="text" name="kode_inventaris" class="form-control date-inputmask" id="kode_inventaris" placeholder="" autocomplete="off" required="required" value="<?=$r['kode_inventaris'];?>" readonly>
                            </div>
                            <div class="form-group m-t-20">
                                <label for="id_petugas">Id Petugas</label>
                                <input type="text" name="id_petugas" class="form-control date-inputmask" id="id_petugas" placeholder="" autocomplete="off" required="required" value="<?=$r['id_petugas'];?>" readonly>
                            </div>
                            <div class="form-group m-t-20">
                                <label for="sumber">Sumber</label>
                                <input type="text" name="sumber" class="form-control date-inputmask" id="sumber" placeholder="" autocomplete="off" required="required" value="<?=$r['sumber'];?>">
                            </div>
                            <div class="form-group m-t-20">
                                <label for="exampleInputFile">Gambar</label>
                                <input type="file" name="foto" id="exampleInputFile" placeholder="" autocomplete="off" value="<?=$r['foto'];?>">
                            </div>
                            <div class="border-top">
                                <div class="card-body">
                                    <button type="submit" name="edit" class="btn btn-primary">Submit</button>
                                </div>
                            </div>
                        </form>
                        <?php
                        if (isset($_POST['edit'])){

                         $nama                =$_POST['nama'];
                         $kondisi            =$_POST['kondisi'];
                         $spesifikasi        =$_POST['spesifikasi'];
                         $keterangan         =$_POST['keterangan'];
                         $jumlah             =$_POST['jumlah'];
                         $id_jenis           =$_POST['id_jenis'];
                         $tanggal_register   =date("Y-m-d H:i:s");
                         $id_ruang           =$_POST['id_ruang'];
                         $kode_inventaris    =$_POST['kode_inventaris'];
                         $id_petugas         =$_POST['id_petugas'];
                         $sumber             =$_POST['sumber'];
                         $foto               =$_POST['foto'];

                         $sql = mysqli_query($koneksi, "UPDATE inventaris SET nama='nama', kondisi='$kondisi', spesifikasi='$spesifikasi', keterangan='$keterangan', jumlah='$jumlah', id_jenis='$id_jenis', tanggal_register='$tanggal_register', id_ruang='$id_ruang', kode_inventaris='$kode_inventaris', id_petugas='$id_petugas',, sumber='$sumber' foto='$foto' WHERE id_inventaris='$id_inventaris'");

                         if (sql){
                           echo "<script>
                           window.alert('Data Berhasil Diedit')
                           window.location='inventaris.php'
                           </script>";
                       } else {
                        echo "Gagal Disimpan";
                    }
                }
                ?>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- footer -->
</div>
<!-- ============================================================== -->
<!-- End Page wrapper  -->
<!-- ============================================================== -->
</div>
<?php
include "footer.php";
?>