hh<!DOCTYPE html>
<html>
<head>
	<title>HELP</title>
</head>
<body>
    <div class="container">
        <div class="card">            
            <div class="card-content">
              <div class="details_big_box">
                <h5 class="center" style="text-decoration: underline;">Prosedur Pemesanan</h5>
                <div>
                  <img src="assets/images/help.png" alt="user" />
                  <ol>
                    <li>1.	Untuk masuk ke halaman login admin dan operator cukup ketikan&nbsp;<span style="font-weight: bold">http://ujikomindah.sknc.site/ujikomindah/ujikomindah/login.php </span> lalu pada link tersebut akan langsung menuju ke halaman login</li><br>

                    <li>Untuk masuk ke halaman login peminjam ketikan<span style="font-weight: bold">http://ujikomindah.sknc.site/ujikomindah/ujikomindah/login.php</span>lalu pilih button pegawai, setelah di klik masukan nip dri pegawai tersebut</li><br>

                    <li>Pada halaman admin, data inventaris dapat dilihat sesuai jurusan. Dan barang diluar jurusan atau bersifat umum seperti proyektor terdapat pada bagian barang lainnya. Pada tabel inventaris terdapat jenis yang berisi nama-nama jurusan yang sudah ada</li><br>

                    <li>Peminjaman barang pada halaman admin dan operator, barang dapat dipinjam sesuai jurusan. Jika ingin meminjam laptop maka bisa membuka pada kelompok jurusan RPL yang kemudian terdapat form untuk di isi. Dan data yang sudah dipinjam akan masuk ke dalam fitur data pinjam yang berisi tabel data peminjaman. Lalu jumlah barang inventaris akan berkurang setelah dipinjam</li><br>

                    <li>Jika barang ingin dikembalikan,konfirmasi kepada admin/operator maka dapat dilihat pada tabel data pinjam dan bisa langsung klik button kembalikan, lalu barang tersebut akan masuk ke dalam tabel pengembalian dan jumlah barang inventaris akan menambah sesuai jumlah yang sudah dikembalikan tadi</li><br>

                    <li>Pada fitur laporan terdapat data yang bisa dicetak melalui cetak excel dan cetak pdf yaitu ada data peminjaman, pengembalian, dan data inventaris</li><br>

                    <li>Pada backup database, database akan langsung terdownload apabila diklik backup</li><br>

                    <li>Di dalam aplikasi inventaris ini, fitur peminjaman,pengembalian dan forgot password masih belum berjalan dengan baik</li>
                </li></ol>
            </div>
        </div>    
    </div>
</div>
</div>
</body>
</html>