<?php 
    function rndStr($api="",$leng=8)
    {
        if (!$api) {
            $use = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890"; 
        }else{
            $use = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890"; 
        }
        srand((double)microtime()*1000000); 
        if ($api) {
            $api .= '-';
        }
        for($i=0; $i<$leng; $i++) { 
          $api .= $use[rand()%strlen($use)]; 
        } 
      return $api; 
    }
?>
