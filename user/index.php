<?php
include "header.php";
?>
<!-- Page wrapper  -->
<!-- ============================================================== -->
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">Data Master</h4>
                <div class="ml-auto text-right">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Library</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!-- End Bread crumb and right sidebar toggle -->
    
    <!-- Container fluid  -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row el-element-overlay">
            <?php
            $pilih=mysqli_query($koneksi,"SELECT * FROM inventaris");
            while($tampil=mysqli_fetch_array($pilih)){
             
                ?>
                <div class="col-lg-3 col-md-3">
                    <div class="card">
                        <div class="el-card-item">
                            <div class="el-card-avatar el-overlay-1"> <img src="../assets/images/barang/<?=$tampil['foto'];?>" style="height: 190px; width: 250px;">
                            </div>
                            <div class="el-card-content">
                                <h4 class="m-b-0"><?=$tampil['nama'];?></h4> <span class="text-muted">Stok : <?=$tampil['jumlah'];?></span><br>
                                <!-- Button trigger modal -->
                                <button ty pe="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#pinjam<?=$tampil['id_inventaris'];?>">
                                  Pinjam
                              </button>

                              <!-- Modal -->
                              <div class="modal fade" id="pinjam<?=$tampil['id_inventaris'];?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                  <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                      <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title" id="myModalLabel"></h4>
                                    </div>
                                    <div class="modal-body">
                                        <form action="proses_pinjamuser.php" method="post">
                                            <input type="hidden" name="id_pegawai" value="<?php echo $_SESSION['pegawai'] ?>">
                                            <input type="hidden" name="id_inventaris" value="<?=$tampil['id_inventaris'] ?>">
                                            <div class="form-group">
                                                <label >Nama Peminjam</label>
                                                <input type="text" class="form-control" name="nama_pegawai" value="<?php echo $_SESSION['nama_pegawai']; ?>" readonly>
                                            </div>
                                            <div class="form-group">
                                                <label>Nama Barang</label>
                                                <input type="text" class="form-control" name="nama" value="<?php echo $tampil['nama'];?>" readonly>
                                            </div>
                                            <div class="form-group">
                                                <label>Jumlah</label>
                                                <input type="number" name="jumlah" max="<?php echo $tampil['jumlah'];?>" value="1">
                                            </div>
                                        </div>

                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            <button type="simpan" name="simpan" value="simpan" class="btn btn-primary">Simpan</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php
    }
    ?>
    
</div>
<!-- ============================================================== -->
<!-- End PAge Content -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Right sidebar -->
<!-- ============================================================== -->
<!-- .right-sidebar -->
<!-- ============================================================== -->
<!-- End Right sidebar -->
<!-- ============================================================== -->
</div><!-- ============================================================== -->
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- footer -->
<!-- ============================================================== -->
<footer class="footer text-center">
    INVENSAPRA SMKN 1 CIOMAS
</footer>
<!-- ============================================================== -->
<!-- End footer -->
<!-- ============================================================== -->
</div>
<!-- End Page wrapper  -->
</div>

<?php
include "footer.php";
?>