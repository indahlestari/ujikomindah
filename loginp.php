<!DOCTYPE html>
<html lang="en">
<head>
	<title>Login</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="login/images/icons/favicon.ico"/>
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="login/vendor/bootstrap/css/bootstrap.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="login/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="login/fonts/iconic/css/material-design-iconic-font.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="login/vendor/animate/animate.css">
	<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="login/vendor/css-hamburgers/hamburgers.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="login/vendor/animsition/css/animsition.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="login/login/vendor/select2/select2.min.css">
	<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="login/vendor/daterangepicker/daterangepicker.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="login/login/css/util.css">
	<link rel="stylesheet" type="text/css" href="login/css/main.css">
	<!--===============================================================================================-->
</head>
<body>
	
	<div class="limiter">
		<div class="container-login100" style="background-image: url('login/images/bg-01.jpg');">
			<div class="wrap-login100">
				<form action="loginp.php" method="post" class="login100-form validate-form">
					<span class="login100-form-logo">
						<i class="zmdi zmdi-landscape"></i>
					</span>

					<span class="login100-form-title p-b-34 p-t-27">
						LOGIN PEMINJAM
					</span>

					<div class="wrap-input100 validate-input" data-validate = "Enter Nip">
						<input class="input100" type="text" name="nip" placeholder="Nip" autocomplete="off">
						<span class="focus-input100" data-placeholder="&#xf207;"></span>
					</div>
					<div class="container-login100-form-btn">
						<button type="submit" name="submit" class="login100-form-btn">
							Login
						</button>
					</div>
				</form>
			</div>

			<?php
			@session_start();
		//Menghubungkan php dengan koneksi DB
			if(isset($_POST['submit'])){
				include 'koneksi.php';

				$nip = $_POST['nip'];

			#Menyeleksi data user dengan username & password yg sesuai
				$login = mysqli_query($koneksi,"SELECT * FROM pegawai WHERE nip='$nip'");

			#Menghitung jumlah data yang ditentukan
				$cek = mysqli_num_rows($login);

			#Cek apakah username dan password ditemukan ada DB
				if ($cek > 0) {
					$data = mysqli_fetch_assoc($login);

			//Cek jika user login sebagao admin
				//buat session login & username
					$_SESSION['pegawai'] = $data['id_pegawai'];
					$_SESSION['nama_pegawai'] = $data ['nama_pegawai'];
					$_SESSION['level'] = "pegawai";

				//Alihkan ke halaman admin
					echo "<script>window.alert('Selamat Datang User')
					window.location='user/index.php'
					</script>";

				//cek jika user login sebagai operator
				} else {
				//Tetap pada halaman login
					echo "<script> window.alert('Maaf anda gagal login')
					window.location='loginp.php'
					</script>";
				}
			}
			?>
			

		</div>
	</div>
	<div id="dropDownSelect1"></div>
	
	<!--===============================================================================================-->
	<script src="login/vendor/jquery/jquery-3.2.1.min.js"></script>
	<!--===============================================================================================-->
	<script src="login/vendor/animsition/js/animsition.min.js"></script>
	<!--===============================================================================================-->
	<script src="login/vendor/bootstrap/js/popper.js"></script>
	<script src="login/vendor/bootstrap/js/bootstrap.min.js"></script>
	<!--===============================================================================================-->
	<script src="login/vendor/select2/select2.min.js"></script>
	<!--===============================================================================================-->
	<script src="login/vendor/daterangepicker/moment.min.js"></script>
	<script src="login/vendor/daterangepicker/daterangepicker.js"></script>
	<!--===============================================================================================-->
	<script src="login/vendor/countdowntime/countdowntime.js"></script>
	<!--===============================================================================================-->
	<script src="login/js/main.js"></script>

</body>
</html>